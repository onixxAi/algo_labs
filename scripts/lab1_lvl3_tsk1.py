import math
a = 0.1
b = 1.0
h = 0.1
x = a
while x <= b:
    s = 1
    temp = 1
    p = 1
    i = 1
    while temp >= 0.0001:
        p = -p
        temp *= ((x * x) / ((4 * (i * i) - 2 * i )))
        s += temp * p
        i += 1
    y = math.cos(x)
    print(f'summ:{round(s, 5)} for x:{round(x, 5)} and y:{round(y, 5)}')
    x += h


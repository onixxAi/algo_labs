import math
a = 0.1
b = 0.8
h = 0.1
x = a
while x <= b:
    i = 1
    s = 0
    temp = 1
    while temp >= 0.0001:
        temp *= x
        l = math.sin((math.pi * i) / 4)
        s += temp * l
        i += 1
    y = (x * math.sin(math.pi / 4)) / (1 - (2 * x * math.cos(math.pi / 4)) + x * x)
    print(f'summ:{round(s, 5)} for x:{round(x, 5)} and y:{round(y, 5)}')
    x += h
